#!/bin/bash

#select the cron path
cron_path=/var/spool/cron/crontabs/root

#cron job to run every 5:00pm.
echo "0 5 * * *  /home/uptimescript.sh" >> $cron_path
