#!/bin/bash

touch /home/uptimelog.csv
echo -e "Date              StartUpTime " | tee -a /home/uptimelog.csv
uptime -s | awk '{print $1 "          " $2}'| tee -a /home/uptimelog.csv
echo -e ""
echo -e "Uptime:  " | tee -a /home/uptimelog.csv
uptime -p | awk '{print $2 " " $3 " " $4 " " $5 " " $6}' | tee -a /home/uptimelog.csv
echo -e  "" | tee -a /home/uptimelog.csv

