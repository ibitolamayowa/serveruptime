# Script to calculate Uptime

This script is meant to calculate the uptime of a machine

## Installation
1. Clone the repository to your home directory
2. Make 'cronjob.sh' executable 
`sudo chmod +x cronjob.sh`
3. Make 'uptimescript.sh' executable
`sudo chmod +x uptimescript.sh`
4. Run the cronjob script
`./cronjob.sh`
5. Run  the uptimescript
`./uptimescript.sh`
6. To see the logs, view uptime.log in the home directory
`cat /home/uptime.log`

## Function
The script updates the log by 5pm everyday


